import random
from prometheus_client import Gauge
from Exporter import Exporter


class RandomExporter(Exporter):

    def __init__(self):
        super().__init__()
        random.seed(1337)

    def set_metrics(self):
        self.value = Gauge("app_random_value", "A random value")

    def fetch_metrics(self):
        self.value.set(random.random())
