import random
from prometheus_client import Gauge
from Exporter import Exporter


class Random09Exporter(Exporter):

    def __init__(self):
        super().__init__()
        random.seed(42)

    def set_metrics(self):
        self.value = Gauge("app_random_single_digit", "A random value between 0 and 9")

    def fetch_metrics(self):
        self.value.set(random.randrange(10))
