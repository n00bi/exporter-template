from abc import abstractmethod


class Exporter:

    def __init__(self):
        self.set_metrics()

    @abstractmethod
    def set_metrics(self): return NotImplementedError

    @abstractmethod
    def fetch_metrics(self): return NotImplementedError
