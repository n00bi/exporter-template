"""Application exporter"""

import os
import time
from prometheus_client import start_http_server

from exporterImpl import RandomExporter, Random09Exporter


class AppMetrics:
    """
    Representation of Prometheus metrics and loop to fetch and transform
    application metrics into Prometheus metrics.
    """

    def __init__(self, polling_interval_seconds=5):
        self.polling_interval_seconds = polling_interval_seconds

        self.exporters = [
            Random09Exporter(),
            RandomExporter()
        ]

    def run_metrics_loop(self):
        """Metrics fetching loop"""

        while True:
            for exporter in self.exporters:
                exporter.fetch_metrics()
            time.sleep(self.polling_interval_seconds)


def main():
    """Main entry point"""

    polling_interval_seconds = int(os.getenv("POLLING_INTERVAL_SECONDS", "5"))
    exporter_port = int(os.getenv("EXPORTER_PORT", "9877"))

    app_metrics = AppMetrics(
        polling_interval_seconds=polling_interval_seconds
    )
    start_http_server(exporter_port)
    app_metrics.run_metrics_loop()


if __name__ == "__main__":
    main()
